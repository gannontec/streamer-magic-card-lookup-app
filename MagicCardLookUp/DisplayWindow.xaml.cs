﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MagicCardLookUp
{
    /// <summary>
    /// Interaction logic for DisplayWindow.xaml
    /// </summary>
    public partial class DisplayWindow : Window
    {
        public DisplayWindow()
        {
            InitializeComponent();
        }
        public void ShowImage(BitmapImage imagescr)
        {
            if (image.Opacity == 1)
            {
                DoubleAnimation fadeOut = new DoubleAnimation
                {
                    From = 1,
                    To = 0,
                    Duration = new Duration(TimeSpan.FromSeconds(1)),
                    AutoReverse = false
                };

                fadeOut.Completed += delegate (object sender, EventArgs e)
                {
                    image.Source = imagescr;
                    DoubleAnimation FadeIn = new DoubleAnimation
                    {
                        From = 0,
                        To = 1,
                        Duration = new Duration(TimeSpan.FromSeconds(1)),
                        AutoReverse = false
                    };
                    image.BeginAnimation(OpacityProperty, FadeIn);
                };
                image.BeginAnimation(OpacityProperty, fadeOut);
            }
            else
            {
                ShowImageWhenHidden(imagescr);
            }
        }

        private void ShowImageWhenHidden(BitmapImage imagescr)
        {
            image.Source = imagescr;
            DoubleAnimation FadeIn = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromSeconds(1)),
                AutoReverse = false
            };
            image.BeginAnimation(OpacityProperty, FadeIn);
        }

       public void HideImage()
        {
            DoubleAnimation da = new DoubleAnimation
            {
                From = 1,
                To = 0,
                Duration = new Duration(TimeSpan.FromSeconds(1)),
                AutoReverse = false
            };
            image.BeginAnimation(OpacityProperty, da);
        }

        private void Display_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}

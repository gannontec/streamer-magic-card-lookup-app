﻿using Scryfall.API;
using Scryfall.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MagicCardLookUp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DisplayWindow displayWindow;
        public Card LastLookup;
        public IList<Card> LastListOfCards;
        public BitmapImage LastLookedUpImage;
        private int _showingFace;
        public MainWindow()
        {
            InitializeComponent();
        }

     

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LastLookup = CardLookUp.LookUpFirstCard(textBox.Text);
                LastListOfCards = CardLookUp.LookUp(textBox.Text);
                if(LastListOfCards.Count >1)
                {
                    comboBox.Items.Clear();
                    for(int i = 1; i < LastListOfCards.Count;i++)
                    {
                        comboBox.Items.Add(LastListOfCards[i].Name);
                    }
                }
                ShowPreview();
            }
            catch(ErrorException ex)
            {
                string messageBoxText = "OOPS cant find any cards,Try a new search";
                string caption = "Totally lost";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;
                MessageBox.Show(messageBoxText, caption, button, icon);
            }
            catch(Exception ex)
            {
                string messageBoxText = "Something Went Wrong, Try Again";
                string caption = "Totally lost";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;
                MessageBox.Show(messageBoxText, caption, button, icon);
            }

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (LastLookedUpImage != null)
            {
                displayWindow.ShowImage(LastLookedUpImage);
            }
            else
            {
                string messageBoxText = "You need to look up a card first";
                string caption = "Totally lost";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Error;
                MessageBox.Show(messageBoxText, caption, button, icon);
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            displayWindow.HideImage();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            displayWindow = new DisplayWindow();
            displayWindow.Show();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //combo box list start and index 1 as 1 is skiped
            LastLookup = LastListOfCards[comboBox.SelectedIndex + 1];
            ShowPreview();
        }
        private void ShowPreview()
        {
            LastLookedUpImage = CardLookUp.GetImageFromURI(CardLookUp.GetImageURIFromCard(LastLookup));
            image.Source = LastLookedUpImage;
        }

        private void buttonFlip_Click(object sender, RoutedEventArgs e)
        {
            if(LastLookup.CardFaces != null && LastLookup.CardFaces.Count >=1 )
            {
                if(_showingFace == 1)
                {
                    _showingFace = 0;
                }
                else
                {
                    _showingFace = 1;
                }
                LastLookedUpImage = CardLookUp.GetImageFromURI(LastLookup.CardFaces[_showingFace].ImageUris.Large);
                image.Source = LastLookedUpImage;
            }
        }
    }


}

﻿using Scryfall.API;
using Scryfall.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MagicCardLookUp
{
  public class CardLookUp
  {
        /// <summary>
        /// Get a Bitmap from uri
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static BitmapImage GetImageFromURI(string uri)
        {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(uri, UriKind.Absolute);
            bitmap.EndInit();
            return bitmap;

        }
        /// <summary>
        /// Get The first card found with the search query  
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static Card LookUpFirstCard(string query)
        {
            var scryfall = new ScryfallClient();

            // Get a random card
            var card = scryfall.Cards.Search(query);
            return card.Data[0];
        }


        /// <summary>
        /// Get The first card found with the search query  
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static IList<Card> LookUp(string query)
        {
            var scryfall = new ScryfallClient();

            // Get a random card
            var card = scryfall.Cards.Search(query);
            return card.Data;
        }

        public static string GetImageURIFromCard(Card card)
        {
            if(card.ImageUris !=null)
            {
                return card.ImageUris.Large;
            }
            else if(card.CardFaces.Count > 0 )
            {
                return card.CardFaces[0].ImageUris.Large;
            }
            return "https://pbs.twimg.com/profile_images/913254465335058433/z9t7SmIM_400x400.jpg";
        }
    }
}
